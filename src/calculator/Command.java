package calculator;

public interface Command {
    int run(int a, int b);
}
