package calculator;

public class PlusCommand  implements Command {
    public int run(int a, int b) {
        assert "calculator.Calculator".equals(Thread.currentThread().getStackTrace()[2].getClassName());
        return a + b;
    }
}
