package calculator;

public class Calculator {
    private static int initCounter = 0;
    private static int setCounter = 0;
    Command _command;
    public Calculator() throws Exception {
        initCounter += 1;
        if (initCounter > 1){
            throw new Exception("you got the limit of constructors");
        }

    }

    public void setCommand(Command _command) throws Exception {
        setCounter += 1;
        if (setCounter > 2){
            throw new Exception("you got the limit of set");
        }
        this._command = _command;
    }

    public int run(int a, int b){
        return this._command.run(a, b);
    }
}
